#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

%:
	dh $@ 

override_dh_auto_build-indep:
	make docs

# For get-orig-source
UURL =  https://github.com/coldtobi/liblightify
PKD   = $(abspath $(dir $(MAKEFILE_LIST)))
PKG   = $(word 2,$(shell dpkg-parsechangelog -l$(PKD)/changelog | grep ^Source))
VER  ?= $(shell dpkg-parsechangelog -l$(PKD)/changelog  -SVersion | sed -e s/^.*git// -e s/+dfsg// | cut -d- -f1 )
DTYPE =
UPSVER ?= $(shell dpkg-parsechangelog -l$(PKD)/changelog -SVersion | cut -d~ -f1)

## from: https://wiki.debian.org/onlyjob/get-orig-source
.PHONY: get-orig-source
get-orig-source: $(PKG)_$(UPSVER)~git$(VER)$(DTYPE).orig.tar.xz
	#$(info I: $(UPSVER$)~$(PKG)_$(VER)$(DTYPE))
	@

UDATE = $(shell date --rfc-3339=seconds --date='TZ="UTC" \
	$(shell echo ~git$(VER) | perl -ne 'print "$$1-$$2-$$3" if m/\+(?:git|svn|hg)(\d{4})(\d{2})(\d{2})/')')

$(PKG)_$(UPSVER)~git$(VER)$(DTYPE).orig.tar.xz:
	@echo DEBUG: VER=$(VER) UDATE=$(UDATE) UPSVER=$(UPSVER)
	@echo "$(PKG)_$(UPSVER)~git$(VER)$(DTYPE).orig.tar.xz"
	@echo $(VER) | grep -q '^[0-9]*$$' || ( echo Cannot find timestamp in package version. Please specify VER, see README.source. Aborting.. ; exit 1 )
	#$(info I: UDATE=$(UDATE))
	$(if $(wildcard $(PKG)-$(UPSVER)~git$(VER)),$(error $(PKG)-$(UPSVER)~git$(VER) exist, aborting..))
	@echo "# Downloading..."
	git clone $(UURL) $(PKG)-$(UPSVER)~git$(VER) \
	|| $(RM) -r $(PKG)-$(UPSVER)~git$(VER)
	cd $(PKG)-$(UPSVER)~git$(VER) \
	&& git checkout v$(VER) || git checkout $$(git log -n1 --format=%h --before="$(UDATE)") \
	&& ( echo "# Generating ChangeLog..." \
	&& git log --pretty="format:%ad  %aN  <%aE>%n%n%x09* %s%n" --date=short > ChangeLog \
	&& touch -d "$$(git log -1 --format='%ci')" ChangeLog) \
	&& echo "# Setting times..." \
	&& for F in $$(git ls-tree -r --name-only HEAD); do touch --no-dereference -d "$$(git log -1 --format="%ai" -- $$F)" "$$F"; done \
	&& echo "# Cleaning-up..." \
	&& $(RM) -r -v $$(awk 'BEGIN{RS=ORS="\n\n";FS=OFS="\n"}/Files-Excluded:/' $(PKD)/copyright | sed -n -e '/^Files-Excluded:/,$${p}' | sed s/Files-Excluded://) \
	&& $(RM) -r .git .git*
	@echo "# Packing..."
	find -L "$(PKG)-$(UPSVER)~git$(VER)" -xdev -type f -print | sort \
	| XZ_OPT="-6v" tar -caf "$(PKG)_$(UPSVER)~git$(VER)$(DTYPE).orig.tar.xz" -T- --owner=root --group=root --mode=a+rX \
	&& $(RM) -r "$(PKG)-$(UPSVER)~git$(VER)"
